FROM cern/c8-base:latest

RUN yum install -y wget && \
    wget https://mirror.openshift.com/pub/openshift-v4/clients/crc/latest/crc-linux-amd64.tar.xz && \
    tar -xf crc-linux-amd64.tar.xz && rm crc-linux-amd64.tar.xz

CMD ["/bin/bash"]
